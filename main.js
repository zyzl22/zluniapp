import Vue from 'vue'
import App from './App'
import api from './common/api'
import store from './store'
import wx from 'weixin-js-sdk'
import share from './common/share.js'


Vue.config.productionTip = false
Vue.prototype.$api = api
Vue.prototype.hostName = 'scrm'
// Vue.prototype.hostName = 'yuedu'
Vue.prototype.apiHost = 'http://' + Vue.prototype.hostName + '.dadi30.com/'
Vue.prototype.mhost = 'http://m.dadi30.com';

App.mpType = 'app'

Vue.prototype.shareWx = share.shareWx;

const app = new Vue({
	store,
	...App
})
app.$mount()
