var wxshare = require('weixin-js-sdk'); //调用微信集成的方法

async function shareWx() {
	let title = arguments[0] ? arguments[0] : '';
	let desc = arguments[1] ? arguments[1] : '';
	let imgUrl = arguments[2] ? arguments[2] : '';
	// let link = arguments[3] ? arguments[3] : '';
	let link = encodeURI(`${this.mhost}/${this.hostName}/#` + this.$route.fullPath);

	// 判断是不是ios系统，是的话截取#前面的路径
	let os = ""
	uni.getSystemInfo({
		success: function(res) {
			os = res.system;
		}
	});
	if (os.indexOf("IOS") === -1) {
		link = link.split("#")[0]
	}

	await this.$api.shareWx({
		url: link
	}).then(res => {

		wxshare.config({
			debug: false, //是否打开调试
			appId: res.data.appId, // 必填，公众号的唯一标识
			timestamp: res.data.timestamp, // 必填，生成签名的时间戳
			nonceStr: res.data.noncestr, // 必填，生成签名的随机串
			signature: res.data.signature, // 必填，签名

			jsApiList: [
				'checkJsApi',
				"updateAppMessageShareData",  //分享到微信及QQ（新接口）
				"updateTimelineShareData",    //分享到朋友圈”及“分享到QQ空间（新接口）
				'onMenuShareWeibo'
			]
		});

		this.$store.dispatch('set_systemData', res.data.data)

		title = title === '' ? res.data.data.gzhname : title;
		desc = desc === '' ? res.data.data.gzhdesc : desc;
		imgUrl = imgUrl === '' ? (res.data.data.apihost + res.data.data.gzhlogo) : imgUrl;

		wxshare.ready(function() {
			//分享给朋友
			wxshare.updateAppMessageShareData({
				title: title, // 分享标题
				desc: desc, // 分享描述
				link: link, // 当前页面链接
				imgUrl: imgUrl, // 分享图标
				success: function() { //分享成功回调
				},
				cancel: function() { //取消分享回调
				}
			});
			//分享到朋友圈
			wxshare.updateTimelineShareData({
				title: title, // 分享标题
				desc: desc, // 分享描述
				link: link, // 当前页面链接
				imgUrl: imgUrl, // 分享图标
				success: function() {},
				cancel: function() {}
			});
		})

		wxshare.error(function(res){
			console.log(
				'error',
				res
			)
		})
	});
}
export default {
	shareWx
}; //暴露出这个方法
