import store from '../store/index.js'
export default function $http(options) {
	const {
		url,
		data
	} = options

	const dataObj = {
		userinfo: store.state.userinfo,
		...data
	}
	return new Promise((reslove, reject) => {
		uni.request({
			url,
			data: dataObj,
			header: {
				'content-type': 'application/x-www-form-urlencoded',
			},
			method: 'POST',
			success(res) {
				reslove(res.data);
			},
			fail(err) {
				reject(err);
			}
		})


	})
}
