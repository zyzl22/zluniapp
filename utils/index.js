// 时间格式化
export const parseTime = (time) => {
	const format = '{y}-{m}-{d} {h}:{i}:{s}'
	let date = null
	if (typeof time === 'string') {
		time = parseInt(time)
	}

	// php时间戳
	time = new Date(parseInt(time) * 1000);

	date = new Date(time)
	const formatObj = {
		y: date.getFullYear(),
		m: date.getMonth() + 1,
		d: date.getDate(),
		h: date.getHours(),
		i: date.getMinutes(),
		s: date.getSeconds()
	}


	const strTime = format.replace(/{(y|m|d|h|i|s+)}/g, (result, key) => {
		let value = formatObj[key]
		if (result.length > 0 && value < 10) {
			value = '0' + value
		}
		return value
	})

	return strTime;
}

//剪切与原始支字符串
export const handleNtoBrSplice = (str, splicenum = 60) => {
	//splicenum要截图的字符数,默认10
	if (str != null) {
		//如果str.length > 60, 则显示...[全文]
		if (str.length > splicenum) {
			return (
				str.slice(0, splicenum).replace(/[\n\r]/g, "<br>") +
				"…"
			);
		} else {
			return str.replace(/[\n\r]/g, "<br>") +
				"…";
		}
	} else {
		return str;
	}
}


// 字符串截取 包含对中文处理,str需截取字符串,start开始截取位置,n截取长度
export const substr = (str, start ,n) => {
	if (str.replace(/[\u4e00-\u9fa5]/g, '**').length <= n) {
	  return str;
	}
	let len = 0;
	let tmpStr = '';
	for (let i = start; i < str.length; i++) { // 遍历字符串
	  if (/[\u4e00-\u9fa5]/.test(str[i])) { // 中文 长度为两字节
	    len += 2;
	  } else {
	    len += 1;
	  }
	  if (len > n) {
	    break;
	  } else {
	    tmpStr += str[i];
	  }
	}
	return tmpStr;
}