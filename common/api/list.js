import $http from '../http.js'

const host = 'scrm'
// const host = 'yuedu'


const httpbase = 'http://'+ host +'.dadi30.com/api.php' 

// 金币充值
export const payOrder = (data) => {
	return $http({
		url: httpbase + '/pay_order/pay',
		data
	})
}

// 修改地址
export const editUserAddress = (data) => {
	return $http({
		url: httpbase + '/wxuser/editUserAddress',
		data
	})
}

// 修改号码, 提交订单, 用户没有号码则保存号码
export const editUserMobile = (data) => {
	return $http({
		url: httpbase + '/wxuser/editUserMobile',
		data
	})
}

// 下单
export const order = (data) => {
	return $http({
		url: httpbase + '/order/store',
		data
	})
}

// 我的订单-产品订单
export const getMyOrder = (data) => {
	return $http({
		url: httpbase + '/order/myOrder',
		data
	})
}

// 我的订单-产品订单
export const getPayOrder = (data) => {
	return $http({
		url: httpbase + '/order/payOrder',
		data
	})
}

// 获取发布的商品
export const getShopping = (data) => {
	return $http({
		url: httpbase + '/shopping/index',
		data
	})
}

// 微信支付
export const wxpay = (data) => {
	return $http({
		url: httpbase + '/wxpay/pay',
		data
	})
}

// 分享给好友
export const shareWx = (data) => {
	return $http({
		url: 'http://scrm.dadi30.com/api.php/wx/shareWx?app=' + host,
		data
	})
}

export const get_system_data = (data) => {
	return $http({
		url: httpbase + '/index/getSystemData',
		data
	})
}

export const get_search = (data) => {
	return $http({
		url: httpbase + '/article/get_search?num=5',
		data
	})
}

// 获取验证码
export const get_yanzhenma = (data) => {
	return $http({
		url: httpbase + '/yanzhen/sendYanzhen',
		data
	})
}

// h5登录(如果没有则注册)
export const login = (data) => {
	return $http({
		url: httpbase + '/wxuser/login',
		data
	})
}

// h5微信登录中转页面
export const tokenLogin = (data) => {
	return $http({
		url: httpbase + '/wxuser/h5login',
		data
	})
}

// token是否有效
export const checkToken = (data) => {
	return $http({
		url: httpbase + '/wxuser/checkToken',
		data
	})
}

// 微信登录(如果没有则注册)
export const wxlogin = (data) => {
	return $http({
		url: httpbase + '/yanzhen/getOpenid',
		data
	})
}

// 绑定手机号码
export const bindTel = (data) => {
	return $http({
		url: httpbase + '/yanzhen/bingTel',
		data
	})
}

// 获取用户未查看的评论总数
export const getNotSeeCommentNum = (data) => {
	return $http({
		url: httpbase + '/message/getNotSeeMessageNum',
		data
	})
}

// 验证手机号码
export const check_mobile = (mobile) => {
	let myreg = /^[1][3,4,5,7,8][0-9]{9}$/;
	return myreg.test(mobile);
}

// 获取用户id刷新用户信息
export const get_user_by_id = (data) => {
	return $http({
		url: httpbase + '/wxuser/getUserFromId',
		data
	})
}

// 获取新闻信息
export const get_list = (data) => {
	return $http({
		url: httpbase + '/article/getList',
		data
	})
}

// 获取商品列表
export const get_shopping = (data) => {
	return $http({
		url: httpbase + '/shopping/list',
		data
	})
}

// 获取信息
export const get_message = (data) => {
	return $http({
		url: httpbase + '/message/getList',
		data
	})
}

// 根据文章id获取评论列表
export const get_comment = (data) => {
	return $http({
		url: httpbase + '/comment/getComment',
		data
	})
}

// 添加评论 addComment
export const add_comment = (data) => {
	return $http({
		url: httpbase + '/comment/addComment',
		data
	})
}

// 浏览数
export const add_viewcount = (data) => {
	return $http({
		url: httpbase + '/article/addViewCount',
		data
	})
}

// 点赞
export const add_zan = (data) => {
	return $http({
		url: httpbase + '/zan/addZan',
		data
	})
}

// 收藏
export const add_collect = (data) => {
	return $http({
		url: httpbase + '/collect/addCollect',
		data
	})
}

// 七牛云获取token
export const get_token_from_qiniu = (data) => {
	return $http({
		url: httpbase + '/qiniu/getToken',
		data
	})
}

// 上传图片到fa_file上，后面做 七牛云删除使用
export const upload_qiniu = (data) => {
	return $http({
		url: httpbase + '/qiniu/upload',
		data
	})
}

// 添加反馈
export const feedback = (data) => {
	return $http({
		url: httpbase + '/feedback/addfeedback',
		data
	})
}

// 发布信息
export const addarc = (data) => {
	return $http({
		url: httpbase + '/article/addarc',
		data 
	})
}


// 发布信息
export const get_info = (data) => {
	return $http({
		url: httpbase + '/index/getInfo',
		data 
	})
}

// 修改昵称和头像
export const editUser = (data) => {
	return $http({
		url: httpbase + '/wxuser/editUser',
		data 
	})
}
