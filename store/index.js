// vuex 状态管理
import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
	// 数据源
	state: {
		userinfo: uni.getStorageSync('USERINFO') || null,
		systemData: uni.getStorageSync('SystemData') || null,
		historyLists: uni.getStorageSync("__hostory") || [],
		notSeeCommentNum: uni.getStorageSync('notSeeCommentNum') || 0
	},
	//可以改变数据源中的数据
	mutations: {
		SET_USER_INFO(state, userinfo) {
			state.userinfo = userinfo
		},
		CLEAR_USER_INFO(state, userinfo) {
			state.userinfo = null
		},
		SET_HISTORY_LISTS(state, history) {
			state.historyLists = history
		},
		CLEAR_HISTORY(state, history) {
			state.historyLists = []
		},
		SET_NOTSEECOMMENTNUM(state, notSeeCommentNum) {
			state.notSeeCommentNum = notSeeCommentNum
		},
		CLEAR_NOTSEECOMMENTNUM(state, notSeeCommentNum) {
			state.notSeeCommentNum = 0
		},
		SET_SYSTEM_DATA(state, systemData){
			state.systemData = systemData
		},
		CLEAR_SYSTEM_DATA(state, systemData){
			state.systemData = null
		}
	},
	actions: {
		set_userinfo({
			commit
		}, userinfo) {
			uni.setStorageSync('USERINFO', userinfo)
			commit('SET_USER_INFO', userinfo)
		},
		clear_userinfo({
			commit
		}) {
			uni.removeStorageSync('USERINFO')
			commit('CLEAR_USER_INFO')
		},
		set_systemData({
			commit
		}, systemData){
			uni.setStorageSync('SystemData', systemData)
			commit('SET_SYSTEM_DATA', systemData)
		},
		clear_systemData({
			commit
		}){
			uni.removeStorageSync('SystemData')
			commit('CLEAR_SYSTEM_DATA')
		},
		set_history({
			commit,
			state
		}, history) {
			let list = state.historyLists
			list.unshift(history)
			uni.setStorageSync('__hostory', list)
			commit('SET_HISTORY_LISTS', list)
		},
		clear_history({
			commit
		}) {
			uni.removeStorageSync('__hostory')
			commit('CLEAR_HISTORY')
		},
		set_notseecommentnum({
			commit
		},notSeeCommentNum){
			uni.setStorageSync('notSeeCommentNum', notSeeCommentNum)
			commit('SET_NOTSEECOMMENTNUM', notSeeCommentNum)
		},
		clear_notseecommentnum({
			commit
		}){
			uni.removeStorageSync('notSeeCommentNum')
			commit('CLEAR_NOTSEECOMMENTNUM')
		}
	}
})

export default store
